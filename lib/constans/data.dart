class Data {
  static const content =
      """Erling Haaland pamer jersey baru Timnas Norwegia. Namun jersey itu langsung jadi omongan, disebut warganet mirip benderanya Thailand atau juga Barcelona!

Lewat media sosial pribadinya, Erling Haaland mem-posting foto dirinya memakai jersey baru Timnas Norwegia. Balutan warna merah, putih, dan biru dari tiap warna dalam detail bendera negaranya terlihat mentereng.

Namun, posting-an Erling Haaland itu jadi omongan warganet di sosial media. Mereka tak sedikit menyebut, jersey tersebut mirip seperti benderanya Thailand!

Untuk diketahui, bendera Norwegia bergambarkan desain salib Nordik yang berwarna latar merah dengan salib berwarna biru yang dibatasi oleh warna putih. Sementara bendera Thailand, memiliki lima lajur mendatar dengan warna berturut-turut seperti merah, putih, biru, putih, dan merah.

Selain itu, tak sedikit juga warganet yang mengomentari kalau jersey baru Timnas Norwegia sekilas mirip-mirip dengan jersey Barcelona!
                  """;
}
