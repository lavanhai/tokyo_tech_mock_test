import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import '../constans/styles.dart';
import 'home_screen.dart';

class BottomNavigationView extends StatefulWidget {
  const BottomNavigationView({super.key});

  @override
  State<BottomNavigationView> createState() => _BottomNavigationViewState();
}

class _BottomNavigationViewState extends State<BottomNavigationView> {
  int _selectedIndex = 0;

  static const List<Widget> _widgetOptions = <Widget>[
    HomeScreen(),
    Center(
      child: Text(
        'Explore',
        style: TextStyle(fontSize: 24, fontWeight: FontWeight.bold),
      ),
    ),
    Center(
      child: Text(
        'Saved',
        style: TextStyle(fontSize: 24, fontWeight: FontWeight.bold),
      ),
    ),
    Center(
      child: Text(
        'Profile',
        style: TextStyle(fontSize: 24, fontWeight: FontWeight.bold),
      ),
    ),
  ];

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  Color _getColorTab(int index) {
    if (_selectedIndex == index) {
      return Colors.blue;
    }
    return const Color(0xff8E8E93);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            const Text(
              "Terkini",
              style: Styles.largeTitle,
            ),
            Container(
              width: 34,
              height: 34,
              decoration: BoxDecoration(
                  color: Colors.grey.withOpacity(0.2),
                  borderRadius: BorderRadius.circular(18)),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  SvgPicture.asset(
                    width: 16,
                    height: 16,
                    // fit: BoxFit.scaleDown,
                    "assets/icons/notification.svg",
                  ),
                ],
              ),
            )
          ],
        ),
      ),
      body: _widgetOptions.elementAt(_selectedIndex),
      bottomNavigationBar: BottomNavigationBar(
        items: <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: SvgPicture.asset(
              "assets/icons/icon_tab_1.svg",
              colorFilter: ColorFilter.mode(_getColorTab(0), BlendMode.srcIn),
            ),
            label: 'Home',
          ),
          BottomNavigationBarItem(
            icon: SvgPicture.asset(
              "assets/icons/icon_tab_2.svg",
              colorFilter: ColorFilter.mode(_getColorTab(1), BlendMode.srcIn),
            ),
            label: 'Explore',
          ),
          BottomNavigationBarItem(
            icon: SvgPicture.asset(
              "assets/icons/icon_tab_3.svg",
              colorFilter: ColorFilter.mode(_getColorTab(2), BlendMode.srcIn),
            ),
            label: 'Saved',
          ),
          BottomNavigationBarItem(
            icon: SvgPicture.asset(
              "assets/icons/icon_tab_4.svg",
              colorFilter: ColorFilter.mode(_getColorTab(3), BlendMode.srcIn),
            ),
            label: 'Profile',
          ),
        ],
        currentIndex: _selectedIndex,
        type: BottomNavigationBarType.fixed,
        selectedItemColor: Colors.blue,
        showUnselectedLabels: true,
        unselectedItemColor: const Color(0xff8E8E93),
        unselectedLabelStyle: const TextStyle(color: Color(0xff8E8E93)),
        onTap: _onItemTapped,
      ),
    );
  }
}
