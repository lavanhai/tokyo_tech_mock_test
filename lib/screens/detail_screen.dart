import 'dart:io';
import 'package:flutter/material.dart';
import 'package:tokyo_tech_mock/constans/data.dart';
import 'package:tokyo_tech_mock/widgets/my_button.dart';
import '../constans/styles.dart';

class DetailScreen extends StatelessWidget {
  const DetailScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        clipBehavior: Clip.antiAlias,
        children: [
          SizedBox(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
          ),
          Image.asset(
            "assets/images/detail_bg.png",
            height: 400,
          ),
          const _AppBar(),
          const _TopContent(),
          const _BodyView()
        ],
      ),
    );
  }
}

class _BodyView extends StatelessWidget {
  const _BodyView({super.key});

  @override
  Widget build(BuildContext context) {
    return Positioned(
      top: 380,
      child: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height * 0.6,
        decoration: BoxDecoration(
            color: Colors.white, borderRadius: BorderRadius.circular(16)),
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.all(16),
              child: Row(
                children: [
                  SizedBox(
                    width: 40,
                    child: Image.asset("assets/images/icon_detail.png"),
                  ),
                  const SizedBox(
                    width: 10,
                  ),
                  Text(
                    "Detik Sport",
                    style: Styles.title3Regular.copyWith(
                        fontWeight: FontWeight.w600, color: Colors.black),
                  ),
                  const Spacer(),
                  _FollowButton()
                ],
              ),
            ),
            Expanded(
              child: SingleChildScrollView(
                  padding:
                      const EdgeInsets.only(left: 16, right: 16, bottom: 30),
                  child: RichText(
                    text: TextSpan(
                        text: "Jakarta - ",
                        style: Styles.content
                            .copyWith(fontWeight: FontWeight.w700),
                        children: const [
                          TextSpan(text: Data.content, style: Styles.content)
                        ]),
                  )),
            )
          ],
        ),
      ),
    );
  }
}

class _TopContent extends StatelessWidget {
  const _TopContent({super.key});

  @override
  Widget build(BuildContext context) {
    return Positioned(
        top: 230,
        child: SizedBox(
          width: MediaQuery.of(context).size.width,
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const SizedBox(
                  height: 21,
                  child: MyButton(
                    title: "Olahraga",
                  ),
                ),
                const SizedBox(
                  height: 8,
                ),
                Text(
                    "Haaland Pakai Jersey Baru Timnas Norwegia... Itu Thailand?",
                    style: Styles.title3Regular.copyWith(height: 1.2)),
                const SizedBox(
                  height: 8,
                ),
                const Text(
                  "21 Mar 2024 • 5 menit membaca",
                  style: Styles.caption1,
                )
              ],
            ),
          ),
        ));
  }
}

class _AppBar extends StatelessWidget {
  const _AppBar({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(
          vertical: Platform.isIOS ? kToolbarHeight : 30, horizontal: 16),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          _RoundButton(
            name: "Button-2",
            onPress: () => Navigator.pop(context),
          ),
          const Row(
            children: [
              _RoundButton(name: "Button-1"),
              SizedBox(
                width: 10,
              ),
              _RoundButton(name: "Button"),
            ],
          )
        ],
      ),
    );
  }
}

class _RoundButton extends StatelessWidget {
  const _RoundButton({super.key, required this.name, this.onPress});

  final String name;
  final VoidCallback? onPress;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: onPress,
        child: Image.asset("assets/images/$name.png", width: 34, height: 34));
  }
}

class _FollowButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 28,
      child: ElevatedButton(
        style: ElevatedButton.styleFrom(
          elevation: 0,
          backgroundColor: const Color(0xffE0E0E0),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(20), // Rounded corners
          ),
        ),
        onPressed: () {
          // Define the action when the button is pressed
        },
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            const Icon(
              Icons.add,
              color: Colors.blue,
              size: 14,
            ), // Plus icon
            const SizedBox(width: 4), // Space between icon and text
            Text(
              'Follow',
              style: Styles.subHeadline.copyWith(color: Colors.blue),
            ),
          ],
        ),
      ),
    );
  }
}
