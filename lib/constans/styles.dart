import 'package:flutter/material.dart';

class Styles {
  static const title2Regular = TextStyle(
      fontSize: 22, fontWeight: FontWeight.w400, color: Colors.white);
  static const title3Regular = TextStyle(
      fontSize: 20, fontWeight: FontWeight.w400, color: Colors.white);
  static const title2Bold= TextStyle(
      fontSize: 22, fontWeight: FontWeight.w700, color: Colors.black);
  static const footNoteRegular = TextStyle(
      fontSize: 13, fontWeight: FontWeight.w400, color: Colors.white);
  static const footNoteBold = TextStyle(
      fontSize: 13, fontWeight: FontWeight.w600, color: Colors.white);
  static const largeTitle = TextStyle(
      fontSize: 34, fontWeight: FontWeight.w700, color: Colors.black);
  static const caption1 = TextStyle(
      fontSize: 12, fontWeight: FontWeight.w400, color: Colors.white);
  static const caption2 = TextStyle(
      fontSize: 11, fontWeight: FontWeight.w600, color: Color(0xff007AFF));
  static const subHeadline = TextStyle(
      fontSize: 15, fontWeight: FontWeight.w400, color: Colors.black);
  static const content = TextStyle(
      fontSize: 17, fontWeight: FontWeight.w400, color: Colors.black);
}
