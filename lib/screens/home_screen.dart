import 'package:flutter/material.dart';
import 'package:tokyo_tech_mock/widgets/my_button.dart';
import '../constans/styles.dart';
import 'detail_screen.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return const Padding(
      padding: EdgeInsets.all(16),
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            _ListIcons(),
            SizedBox(
              height: 24,
            ),
            _BannerView()
          ],
        ),
      ),
    );
  }
}

class _ListIcons extends StatelessWidget {
  const _ListIcons({super.key});

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 60,
      child: ListView(
        shrinkWrap: true,
        scrollDirection: Axis.horizontal,
        children: const [
          _RoundImage(
            imageName: "Group 1",
          ),
          _RoundImage(
            imageName: "Group 2",
          ),
          _RoundImage(
            imageName: "Group 3",
          ),
          _RoundImage(
            imageName: "Group 4",
          ),
          _RoundImage(
            imageName: "Group 5",
          )
        ],
      ),
    );
  }
}

class _BannerView extends StatelessWidget {
  const _BannerView({super.key});

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: MediaQuery.of(context).size.height -
          100, // Adjust the height as needed
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const Text(
            "Berita terbaru",
            style: Styles.title2Bold,
          ),
          const SizedBox(
            height: 16,
          ),
          Stack(
            children: [
              Image.asset("assets/images/banner.png",
                  height: 204, width: double.maxFinite, fit: BoxFit.fill),
              Positioned(
                right: 16,
                top: 16,
                child: SizedBox(
                  height: 21,
                  child: MyButton(
                    title: "Teknologi",
                    textStyle: Styles.caption1
                        .copyWith(fontSize: 11, fontWeight: FontWeight.w600),
                  ),
                ),
              ),
              Positioned(
                  bottom: 16,
                  left: 16,
                  right: 16,
                  child: SizedBox(
                    width: MediaQuery.of(context).size.width,
                    child: Column(
                      children: [
                        Text(
                          "Threads mencatat 100 juta pengguna kurang dari satu pekan",
                          style: Styles.title3Regular.copyWith(height: 1.2),
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        Row(
                          children: [
                            Image.asset(
                              "assets/images/new.png",
                              width: 20,
                              height: 20,
                            ),
                            const SizedBox(
                              width: 10,
                            ),
                            const Text(
                              "BBC News • 21 Mar 2024",
                              style: Styles.caption1,
                            )
                          ],
                        )
                      ],
                    ),
                  ))
            ],
          ),
          const SizedBox(
            height: 24,
          ),
          const Expanded(
            child: _BottomList(),
          )
        ],
      ),
    );
  }
}

class _BottomList extends StatefulWidget {
  const _BottomList({super.key});

  @override
  State<_BottomList> createState() => _BottomListState();
}

class _BottomListState extends State<_BottomList> {
  List<Map<String, dynamic>> listData = [
    {
      "title": "Olahraga",
      "content": "Haaland Pakai Jersey Baru Timnas Norwegia... Itu Thailand?",
      "avatar": "avatar_1.png",
      "icon": "Group 2.png",
      "time": "Detik Sport • 21 Mar 2024"
    },
    {
      "title": "Politik",
      "content": "Hasil final rekapitulasi suara Pilpres dan Pileg 2024",
      "avatar": "avatar_2.png",
      "icon": "Group 1.png",
      "time": "BBC News • 21 Mar 2024"
    },
    {
      "title": "Otomotif",
      "content": "Mudik Lebaran: Mengenal Komponen Fast Moving Mobil",
      "avatar": "avatar_3.png",
      "icon": "Group 3.png",
      "time": "Tempo • 21 Mar 2024"
    },
  ];

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const Text("Untukmu", style: Styles.title2Bold),
        const SizedBox(
          height: 16,
        ),
        Expanded(
          child: ListView.builder(
              shrinkWrap: true,
              physics: const NeverScrollableScrollPhysics(),
              itemCount: listData.length,
              itemBuilder: (context, index) => _ItemRow(
                    item: listData[index],
                  )),
        )
      ],
    );
  }
}

class _ItemRow extends StatelessWidget {
  const _ItemRow({super.key, required this.item});

  final Map<String, dynamic> item;

  void _onPressItem(BuildContext context) {
    Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => const DetailScreen(),
        ));
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => _onPressItem(context),
      child: Padding(
        padding: const EdgeInsets.only(bottom: 16),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Image.asset(
              "assets/images/${item["avatar"]}",
              width: 90,
              height: 90,
            ),
            const SizedBox(
              width: 16,
            ),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    item["title"],
                    style: Styles.caption2,
                  ),
                  Text(
                    item["content"],
                    style: Styles.subHeadline.copyWith(height: 1.2),
                  ),
                  const SizedBox(
                    height: 8,
                  ),
                  Row(
                    children: [
                      Image.asset(
                        "assets/images/${item["icon"]}",
                        width: 20,
                        height: 20,
                      ),
                      const SizedBox(
                        width: 10,
                      ),
                      Text(
                        item["time"],
                        style: Styles.caption1
                            .copyWith(color: const Color(0xff8E8E93)),
                      ),
                    ],
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}

class _RoundImage extends StatelessWidget {
  const _RoundImage({super.key, required this.imageName});

  final String imageName;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(right: 10),
      child: Image.asset(
        "assets/images/$imageName.png",
        width: 60,
        height: 60,
      ),
    );
  }
}
