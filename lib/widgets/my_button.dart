import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:tokyo_tech_mock/constans/styles.dart';

class MyButton extends StatelessWidget {
  const MyButton({super.key, this.onPress, this.title, this.textStyle});

  final VoidCallback? onPress;
  final String? title;
  final TextStyle? textStyle;

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
        style:  const ButtonStyle(
          padding: MaterialStatePropertyAll(EdgeInsets.symmetric(horizontal: 4)),
            backgroundColor: MaterialStatePropertyAll(Color(0xff0007aff))),
        onPressed: onPress,
        child: Text(
          title ?? "",
          style: textStyle ?? Styles.caption1,
        ));
  }
}
