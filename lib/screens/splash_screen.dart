import 'package:flutter/material.dart';
import '../constans/styles.dart';
import '../widgets/my_button.dart';
import 'bottom_navigation_view.dart';

class SplashScreen extends StatelessWidget {
  const SplashScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
        child: Stack(
          children: [
            Image.asset(
              "assets/images/home_bg.png",
              fit: BoxFit.cover,
              width: double.maxFinite,
              height: double.maxFinite,
            ),
            const _TopPagination(),
            const Positioned(top: 16, left: 8, child: _Title()),
            const Positioned(
              bottom: 80,
              left: 16,
              child: _BodyView(),
            )
          ],
        ),
      ),
    );
  }
}

class _TopPagination extends StatelessWidget {
  const _TopPagination({super.key});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 8.0),
      child: SizedBox(
        height: 16,
        child: Row(
          children: [
            Expanded(
              child: Container(
                height: 2,
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(5)),
              ),
            ),
            const SizedBox(
              width: 10,
            ),
            Expanded(
              child: Container(
                height: 2,
                decoration: BoxDecoration(
                    color: Colors.white.withOpacity(0.8),
                    borderRadius: BorderRadius.circular(5)),
              ),
            ),
            const SizedBox(
              width: 10,
            ),
            Expanded(
              child: Container(
                height: 2,
                decoration: BoxDecoration(
                    color: Colors.white.withOpacity(0.6),
                    borderRadius: BorderRadius.circular(5)),
              ),
            ),
            const SizedBox(
              width: 10,
            ),
            Expanded(
              child: Container(
                height: 2,
                decoration: BoxDecoration(
                    color: Colors.white.withOpacity(0.4),
                    borderRadius: BorderRadius.circular(5)),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class _Title extends StatelessWidget {
  const _Title({super.key});

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Container(
            width: 32,
            height: 32,
            decoration: BoxDecoration(
                color: Colors.white, borderRadius: BorderRadius.circular(16)),
            child: Image.asset("assets/images/pngwing 1.png")),
        const SizedBox(
          width: 10,
        ),
        const Text(
          "detikcom",
          style: Styles.footNoteBold,
        ),
        const SizedBox(
          width: 10,
        ),
        Text("5 minute",
            style: Styles.footNoteRegular
                .copyWith(color: Colors.white.withOpacity(0.4))),
      ],
    );
  }
}

class _BodyView extends StatelessWidget {
  const _BodyView({super.key});

  void _onNextScreen(BuildContext context) {
    Navigator.pushReplacement(
        context,
        MaterialPageRoute(
          builder: (context) => const BottomNavigationView(),
        ));
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: double.maxFinite,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(
            height: 24,
            child: MyButton(
              title: "Olahraga",
              onPress: () => _onNextScreen(context),
            ),
          ),
          const SizedBox(
            height: 8,
          ),
          SizedBox(
            width: MediaQuery.of(context).size.width * 0.89,
            child: const Text(
              "Statistik Idzes Vs Vietnam: Solid dan Tenang di Belakang",
              style: Styles.title2Regular,
            ),
          ),
          SizedBox(
            width: MediaQuery.of(context).size.width * 0.8,
            child: const Text(
              "22 Mar 2024 • 5 menit membaca",
              style: Styles.footNoteRegular,
            ),
          ),
          const SizedBox(
            height: 8,
          ),
          SizedBox(
            width: MediaQuery.of(context).size.width * 0.8,
            child: const Text(
              "Jay Idzes tampil apik menjalani debut bersama Timnas Indonesia saat mengalahkan Vietnam.",
              style: Styles.footNoteRegular,
            ),
          ),
          const SizedBox(
            height: 8,
          ),
          SizedBox(
            width: MediaQuery.of(context).size.width * 0.8,
            child: Text(
              "Baca Selengkapnya",
              style: Styles.footNoteRegular
                  .copyWith(color: const Color(0xffFFCC00)),
            ),
          ),
        ],
      ),
    );
  }
}
